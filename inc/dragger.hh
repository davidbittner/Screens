#ifndef DRAGGER_HEADER
#define DRAGGER_HEADER

#include <gtkmm/window.h>
#include <thread>
#include <atomic>

#include "screenshot.hh"
#include "rect.hh"

class Dragger : public Gtk::Window
{
public:
     Dragger();
    ~Dragger();

    bool isDone();

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context> &cont) override;

private:
    int x1, x2, y1, y2;

    ScreenShot  *cropShot;
    std::mutex cropShot_mutex;

    //Threads for uploading and writing asynchronously
    std::future<bool> uploadThread;
    std::future<bool> writeThread;
    std::future<bool> encodeThread;
    std::future<void> waiterThread;

    //Atomic markers for when each thread is finished
    std::atomic<bool> writeDone;
    std::atomic<bool> uploadDone;
    bool both;

    Rectangle selectArea;
    bool mousePress(GdkEventButton *event);

    void process();
};


#endif
