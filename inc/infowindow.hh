#ifndef INFO_WINDOW_HEADER
#define INFO_WINDOW_HEADER

#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/box.h>
#include <gtkmm/drawingarea.h>
#include <gtkmm/liststore.h>

#include "imagelistmodel.hh"
#include "resizingimage.hh"

#include <functional>

class InfoWindow : public Gtk::VBox 
{
public:
    InfoWindow(Glib::RefPtr<Gtk::ListStore> &list, ImageListModel *model);

    void setInfo(const Gtk::TreeModel::Path &path);
private:
    void deletePressed();

    Gtk::Entry  url, date;
    Gtk::Button deleteButton;

    ResizingImage preview;

    Glib::RefPtr<Gtk::ListStore> &imageListStore;
    Gtk::TreeIter  currentSelection;
    Gtk::TreeRow   curRow;
    ImageListModel *model;
};

#endif
