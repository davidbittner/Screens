#include "argmanager_types.hh"

using namespace argmanager;

const char *arg_exception::what() const throw() {
    return error.c_str(); 
}

arg_exception::arg_exception(std::string arg_name, std::string error) {
    this->arg_name = arg_name;
    this->error = error;
}

std::string arg_exception::print() {
    return arg_name + ": " + error;
}
