#ifndef SETTINGS_HEADER
#define SETTINGS_HEADER

#include "json.hpp"
#include "screenshot.hh"

#include <filesystem>

#define SAVE_PATH  "savepath"
#define AUTH_TOKEN "authToken"
#define FILE_TYPE  "filetype"

#define IMAGE_LIST "screens"

using json = nlohmann::json;

class ResourceManager
{
public:
    ResourceManager();

    json imageDump();

    void addScreen(ScreenShot &new_screen);
    void newImageList(json imgs);

    std::string getSettingsValue(std::string val);
    void        setSettingsValue(std::string val, std::string set);

    void updateSettings();

    void saveImages();
    void saveSettings();
private:
    json settings;
    json image_list;

    json getDefaults();

    std::string home_dir;

    json readJsonFile(std::filesystem::path &path);
};

#endif
