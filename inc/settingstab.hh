#ifndef SETTINGS_TAB_HEADER
#define SETTINGS_TAB_HEADER

#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>

#include <vector>
#include <string>

#include "settings.hh"

class SettingsTab : public Gtk::VBox
{
public:
    SettingsTab();

    class SettingsEntry {
        public:
            SettingsEntry(std::string name, std::string setting_name);
            void add(Gtk::VBox &cont);

            void save(ResourceManager &to);

        private:
            Gtk::HBox holder;
            Gtk::Entry val;
            Gtk::Label label;

            std::string setting_name;
    };

private:
    std::vector<SettingsEntry> settings_list;

    void savePressed();

    Gtk::Button saveButton;
};

#endif
