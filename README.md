![logo](https://gitlab.com/davidbittner/Screens/raw/master/assets/logo.png)
[![pipeline status](https://gitlab.com/davidbittner/Screens/badges/master/pipeline.svg)](https://gitlab.com/davidbittner/Screens/commits/master)
# Screens
Screens is an application that makes it easier to take screenshots in Linux. It is a Linux alternative to ShareX currently in early development stages.

![screenshot](https://gitlab.com/davidbittner/Screens/raw/master/assets/screen.png)

## What works?
+ Screenshots
+ Automatic Upload to Imgur
+ URL Injection to Clipboard (kinda)
+ Screenshot cropping (select a region)
+ Keeping a list of uploaded images with their URLs
+ User-friendly UI using GTK

## Known Issues
+ Some WM's don't like me editing the clipboard, fix pending
+ Some WM's (specifically i3) don't render background windows when item is full screened. This only effects the dragging mode, which works, but can be tedious to use.

## What's planned?
+ Alternative upload sites (maybe idk)
+ Any feature requests you guys may have!

## Compiling
Screens requires all of the following dependencies to compile:
+ gtkmm-3.0
+ gstreamermm-1.0
+ libx11
+ libpng
+ libcurl
+ [Json for Modern C++](https://github.com/nlohmann/json)
 + This library is genuinely incredible, go check it out
+ [lodepng](http://lodev.org/lodepng/)
 + Nice little single file PNG encoder/decoder. Likely will be replaced by my own routine with libpng, but nice nonetheless.

To compile, simply generate the makefiles/project files for your build system. In Linux, use

```
cmake -G "Unix Makefiles"
make
```

## Installation

Currently, the only package for Screens is on the AUR. There is one that follows `master` which is `screens-git`. The one that contains the most up-to-date binary distribution is just called `screens`.

If you would like to see it on your favorite package manager, I'd be happy to try and write a package for it if I get enough requests. Additionally, you're always willing to write your own, just please send me the file so I can distribute it to others.

## Usage
When launching the program, it can take a few command line arguments. These arguments determine what the program does. All functionality is packaged within the single executable.

### --screen
+ This tells it to take a screenshot. Without this, it will open the image list GUI to see past images that have been uploaded online.

+ --write
 + This argument can only be given if --screen has been supplied. This tells it to write to disk to the path given in the config file.
+ --upload
 + This argument can also only be given if --screen has been given. This tells it to upload the file to imgur. Immediately after upload you can press Ctl+V to paste the URL.
+ --drag
 + This argument can only be given if taking a screenshot. It tells the program you would like to use a drag box to crop the image that you are taking.

### No arguments
If no arguments are given, it will launch the normal UI to view past screenshots that have been uploaded. It also gives you a tab for editing settings.

Additionally, when an image is deleted using this UI, it is deleted on Imgur itself, not just from your list.

