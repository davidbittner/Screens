#include "resizingimage.hh"

#include <iostream>
#include <fstream>
#include <giomm/inputstream.h>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

#include <sstream>
#include <cstdlib>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

static auto logger = spdlog::stdout_color_mt("resizer");

ResizingImage::ResizingImage()
{
}

bool ResizingImage::set(std::string url)
{
    auto stream = Gio::MemoryInputStream::create();
    std::stringstream read;

    curlpp::Cleanup cleaner;
    curlpp::Easy request;

    request.setOpt(new curlpp::options::Url(url));
    request.setOpt(new curlpp::options::WriteStream(&read));
    request.perform();

    stream->add_data(read.str().c_str(), read.str().size());

    try{
        image = Gdk::Pixbuf::create_from_stream(stream);
        queue_draw();

        return true;
    }catch(Glib::Error er)
    {
        std::string msg = er.what();
        logger->error("{}", msg);

        image.reset();
        return false;
    }
}

void ResizingImage::set()
{
    image.reset();
    queue_draw();
}

bool ResizingImage::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    if(image)
    {
        //This headache is just for scaling/centering image
        auto alloc = get_allocation();

        int w = 0;
        int h = 0;

        double aspect = (double)image->get_width()/image->get_height();

        if(((double)alloc.get_width()/alloc.get_height()) > aspect)
        {
            h = alloc.get_height();
            w = h * (aspect);
        }else{
            w = alloc.get_width();
            h = w * (1/aspect);
        }

        auto resizedImage = image->scale_simple(w,
                                                h, 
                                                Gdk::INTERP_BILINEAR);

        Gdk::Cairo::set_source_pixbuf(cr, 
                                      resizedImage,
                                      (alloc.get_width()-w)/2,
                                      (alloc.get_height()-h)/2);
        cr->rectangle(0, 
                      0, 
                      resizedImage->get_width()+((alloc.get_width()-w)/2),
                      resizedImage->get_height()+((alloc.get_height()-h)/2));
        cr->fill(); 
    }
    
    return true;
}
