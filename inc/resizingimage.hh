#ifndef RESIZING_IMAGE_HEADER
#define RESIZING_IMAGE_HEADER

#include <gtkmm/drawingarea.h>
#include <gdkmm/pixbuf.h>
#include <gtkmm.h>

class ResizingImage : public Gtk::DrawingArea
{
public:
    ResizingImage();

    bool set(std::string newImage);
    void set();

private:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
    Glib::RefPtr<Gdk::Pixbuf> image;
};

#endif
