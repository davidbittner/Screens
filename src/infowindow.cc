#include "infowindow.hh"

#include <gtkmm/alignment.h>
#include <gtkmm/stock.h>
#include <gtkmm/frame.h>
#include <gtkmm/scrolledwindow.h>

#include <iostream>

#include "curlpp/cURLpp.hpp"
#include "curlpp/Easy.hpp"
#include "curlpp/Options.hpp"

#include "settings.hh"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

static auto logger = spdlog::stdout_color_mt("infowindow");

InfoWindow::InfoWindow(Glib::RefPtr<Gtk::ListStore> &list, ImageListModel *model) :
    deleteButton("Delete"),
    imageListStore(list),
    currentSelection(nullptr)
{
    this->model = model;

    set_homogeneous(false);

    url.set_text("URL: ");
    date.set_text("Date Taken: ");

    url.set_editable(false);
    date.set_editable(false);

    pack_start(preview, Gtk::PACK_EXPAND_WIDGET, 5);
    pack_start(url, Gtk::PACK_SHRINK, 5);
    pack_start(date, Gtk::PACK_SHRINK, 5);
    pack_start(deleteButton, Gtk::PACK_SHRINK, 5);

    set_margin_left(5);
    set_margin_right(5);

    deleteButton.signal_pressed().connect(sigc::mem_fun(*this, &InfoWindow::deletePressed));
}

void InfoWindow::setInfo(const Gtk::TreeModel::Path &path)
{
    curRow           = *imageListStore->get_iter(path);
    currentSelection =  imageListStore->get_iter(path);

    std::string urlString = curRow[model->url];
    std::string dateString = curRow[model->date];
    if(urlString.size() == 0)
    {
        url.set_text("URL: N/A");
    }else
    {
        url.set_text("URL: " + urlString);
    }

    date.set_text("Date Taken: " + dateString);
    if(!preview.set(urlString))
    {
        url.set_text("URL: ");
        date.set_text("Filename: ");

        auto app_sender = Gio::Application::create("io.bittwise.screens.notif", Gio::APPLICATION_FLAGS_NONE);
        app_sender->register_application();

        auto notif = Gio::Notification::create("Failed to Load");
        
        notif->set_body("Screenshot at: "+ urlString + " not found. Check your connection.");
        app_sender->send_notification(notif); 
    }
}

bool deleteImage(std::string deleteHash)
{
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

    using namespace curlpp::options;
    std::string url = "https://api.imgur.com/3/image/"+deleteHash;

    std::string auth_token = ResourceManager().getSettingsValue("authToken");
    auth_token = "Authorization: " + auth_token;

    std::list<std::string> headers;
    headers.push_back(auth_token);

    request.setOpt<CustomRequest>("DELETE");
    request.setOpt<HttpHeader>(headers);
    request.setOpt<Url>(url);

    std::stringstream stream;
    stream << request;

    std::string res = stream.str();

    json j = json::parse(res);

    if(j["success"] == true)
    {
        ResourceManager image_list;
        json j = image_list.imageDump();
        for (json::iterator it = j.begin(); it != j.end(); ++it) {
            if((*it)["delete"] == deleteHash)
            {
                j.erase(it);
                image_list.newImageList(j);
                image_list.saveImages();
                return true;
            }
        }
    }else
    {
        std::cerr << "Failed to delete image." << std::endl;
        std::cout << j.dump(4) << std::endl;
        return false;
    }
    return false;
}

void InfoWindow::deletePressed()
{   
    if(currentSelection)
    {
        std::string deleteHash = curRow[model->deleteHash];
        
        if(deleteImage(deleteHash))
        {
            url.set_text("URL: ");
            date.set_text("Filename: ");
            
            preview.set();

            imageListStore->erase(currentSelection);
            currentSelection = Gtk::TreeIter();
        }
    }
}
