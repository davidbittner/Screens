#ifndef IMAGE_LIST_MODEL_HEADER
#define IMAGE_LIST_MODEL_HEADER

#include <gtkmm/treemodel.h>
#include <string>

class ImageListModel : public Gtk::TreeModelColumnRecord
{
public:
    ImageListModel();

    Gtk::TreeModelColumn<std::string> date;
    Gtk::TreeModelColumn<std::string> url;
    Gtk::TreeModelColumn<std::string> deleteHash;
};

#endif
