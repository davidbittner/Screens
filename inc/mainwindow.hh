#ifndef SCREEN_HEADER
#define SCREEN_HEADER

#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/notebook.h>
#include <gtkmm/treeview.h>
#include <gtkmm/paned.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>

#include <future>

#include "json.hpp"
#include "imagelistmodel.hh"
#include "infowindow.hh"
#include "settingstab.hh"

using json = nlohmann::json;

class MainWindow : public Gtk::Window
{
public:
    MainWindow();
    virtual ~MainWindow();

private:
    void changeTab(Gtk::Widget *page, guint pageNum );
    void rowChanged(const Gtk::TreeModel::Path &path, Gtk::TreeViewColumn *column);

    SettingsTab settingsTab;

    Gtk::Paned    imageTab;
    Gtk::Box      imageInfoBox;

    Glib::RefPtr<Gtk::ListStore> imageListStore;
    ImageListModel      imageModel;

    Gtk::TreeView       imageList;
    Gtk::ProgressBar    loadingBar;
    Gtk::ScrolledWindow imageListHolder;

    Gtk::Notebook notebook;

    InfoWindow infoWindow;

};

#endif
