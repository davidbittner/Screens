#include "settings.hh"
#include <cstdlib>
#include <fstream>
#include <iostream>

#include <giomm.h>

#define SETTINGS_PATH "/.config/screens/settings.json"
#define LIST_PATH     "/.local/share/screens/image_list.json"

ResourceManager::ResourceManager()
{
    home_dir = std::getenv("HOME");
    updateSettings(); 
}

void ResourceManager::updateSettings()
{
    std::filesystem::path settings_path(home_dir + SETTINGS_PATH);
    std::filesystem::path list_path(home_dir + LIST_PATH);

    settings = getDefaults();
    if(std::filesystem::exists(settings_path))
    {
        auto temp = readJsonFile(settings_path);
        for (json::iterator it = temp.begin(); it != temp.end(); ++it) {
            temp[it.key()] = it.value();
        }
        
        saveSettings();
    } else
    {
        std::filesystem::create_directories(settings_path.parent_path());
        saveSettings();
    }

    if(std::filesystem::exists(list_path))
    {
        image_list = readJsonFile(list_path);
    } else
    {
        std::filesystem::create_directories(list_path.parent_path());
        image_list = {
            {"screens", json::array()} 
        };
    }
}

json ResourceManager::readJsonFile(std::filesystem::path &path)
{
    std::ifstream read(path);
    json j;

    read >> j;

    return j;
}

json ResourceManager::getDefaults()
{
    json ret;

    ret[SAVE_PATH]  = home_dir+"/Pictures/";
    ret[AUTH_TOKEN] = "Client-ID 52cf422cf3c9636";
    ret[FILE_TYPE]  = "png";

    return ret;
}

json ResourceManager::imageDump() 
{
    return image_list[IMAGE_LIST];
}

void ResourceManager::newImageList(json imgs)
{
    image_list["screens"] = imgs;
}

std::string ResourceManager::getSettingsValue(std::string val)
{
    return settings[val];
}

void ResourceManager::setSettingsValue(std::string val, std::string set)
{
    settings[val] = set;
}

void ResourceManager::saveSettings()
{
    std::string json_str = settings.dump(4); 

    std::ofstream output(home_dir + SETTINGS_PATH);
    output << json_str;

    output.close();
}

void ResourceManager::saveImages()
{
    std::string json_str = image_list.dump(4); 

    std::ofstream output(home_dir + LIST_PATH);
    output << json_str;

    output.close();
}

void ResourceManager::addScreen(ScreenShot &new_screen)
{    
    json &images = image_list["screens"];
    images.push_back(new_screen);

    saveImages();

    json info;
    to_json(info, new_screen);

    auto app_sender = Gio::Application::create("io.bittwise.screens.notif", Gio::APPLICATION_FLAGS_NONE);
	app_sender->register_application();
    auto notif = Gio::Notification::create("Screenshot Uploaded");
    notif->set_body("Screenshot URL:\n"+info["url"].get<std::string>());
    app_sender->send_notification(notif);
}
