#pragma once

#include <string>
#include <map>
#include <optional>
#include <iostream>

#include "argmanager_types.hh"

using namespace argmanager;

#ifndef COMP_VERSION
    #define VER_STR "0"
#else
    #define VER_STR COMP_VERSION
#endif

class ArgumentManager
{
public:
    ArgumentManager(std::string prog_name, std::string desc);
    void passArgs(int argc, char **argv);

    void addArg(std::string name, std::string desc, bool required, bool has_arg);
    void addArg(std::string s_name, std::string l_name, std::string desc, bool required, bool has_arg);

    //must pass short name
    bool hadArg(std::string name);

    std::string getArg(std::string name);

    void printHelp(std::ostream &str);
private:
    //args are all stored with the short name, as that's the name that always exists
    std::map<std::string, arg_info> expected_args;
    std::map<std::string, arg>      passed_args;

    std::string prog_name;
    std::string desc;

    std::string ver = VER_STR;
};

ArgumentManager &StaticManager();
