#include "argmanager.hh"

#include <iostream>
#include <algorithm>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

static auto logger = spdlog::stdout_color_mt("args");

using namespace argmanager;

ArgumentManager::ArgumentManager(std::string prog_name, std::string desc) {
    this->prog_name = prog_name;
    this->desc = desc;
}

void ArgumentManager::addArg(std::string name, std::string desc, bool required, bool has_arg) {
    expected_args[name] = arg_info{
        required,
        has_arg,

        name,
        "",

        desc
    };
}

void ArgumentManager::addArg(std::string s_name, std::string l_name, std::string desc, bool required, bool has_arg) {
    expected_args[l_name] = arg_info{
        required,
        has_arg,

        s_name,
        l_name,

        desc
    };
}

void ArgumentManager::passArgs(int argc, char **argv) {
    std::vector<std::string> passed;
    for(int i = 1; i < argc; i++) {
        std::string name(argv[i]);
        passed.push_back(name);

        //if it doesn't match the s_name, see if it matches the l_name
        auto loc = expected_args.find(name);
        if(loc == expected_args.end()) {
            auto loc = std::find_if(expected_args.begin(), expected_args.end(), [&](auto &key) {
                if(name == key.second.s_name) {
                    return true;
                }
                return false;
            });

            if(loc == expected_args.end()) {
                throw arg_exception(
                    name,
                    "unknown arg passed"
                );    
            }else{
                passed.at(passed.size()-1) = loc->second.l_name;
            }
        }
    }

    for(int i = 0; i < passed.size(); i++) {
        if(passed.at(i)[0] == '-') {
            int temp_i = i+1;
            if(temp_i >= passed.size() || passed.at(temp_i)[0] == '-') {
                passed_args[passed.at(i)] = arg{
                    {}
                };
            }else{
                passed_args[passed.at(i)] = arg{
                    passed.at(temp_i)
                };
            }
        }
    }

    for(auto &info : expected_args) {
        auto loc = passed_args.find(info.second.s_name);

        if(info.second.required && loc == passed_args.end()) {
            throw arg_exception(
                loc->first,
                "missing required argument"
            );
        }else if(loc == passed_args.end()){
            continue;
        }else{
            if(info.second.has_arg && !loc->second.param) {
                throw arg_exception(
                    loc->first,
                    "missing required parameter"
                );
            }
        }
    }
}

bool ArgumentManager::hadArg(std::string name) {
    auto loc = passed_args.find(name);
    if(loc == passed_args.end()) {
        return false;
    }
    return true;
}

std::string ArgumentManager::getArg(std::string name) {
    auto loc = passed_args.find(name);
    if(loc == passed_args.end()) {
        throw arg_exception(
            name,
            "argument was not passed"
        );
    }else{
        return *loc->second.param;
    }
}

void ArgumentManager::printHelp(std::ostream &str) {
    str << prog_name << "-" << ver << " - " << desc << "\n";
    str << "Usage: " << prog_name << " [options...]\n";
    for(auto &com : expected_args) {
        str << "\t" << com.second.s_name;
        if(com.second.l_name != "") {
            str << ", " << com.second.l_name;
        }

        if(com.second.has_arg) {
            str << " [arg]";
        }

        str << " - " << com.second.desc << "\n";
    }
}

ArgumentManager &StaticManager()
{
    static ArgumentManager *argManager = new ArgumentManager("screens", "A utility for taking and managing screenshots");
    return *argManager;
}
