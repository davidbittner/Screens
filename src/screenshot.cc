#include "screenshot.hh"

#include <ctime>
#include <iomanip>
#include <cstring>
#include <iostream>
#include <sstream>
#include <iterator>
#include <array>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <filesystem>

#include <gdkmm/screen.h>

#include "base64.hh"

#include "settings.hh"

#include "curlpp/cURLpp.hpp"
#include "curlpp/Easy.hpp"
#include "curlpp/Options.hpp"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include <gdkmm/display.h>

static auto logger = spdlog::stdout_color_mt("screenshot");

ScreenShot::ScreenShot(SCREEN_TYPE type)
{
    Glib::RefPtr<Gdk::Window> wind;
    auto full_screen = Gdk::Screen::get_default(); 

    bool crop = false;
    Gdk::Rectangle crop_pos;

    switch(type) {
        case SCREEN_TYPE::CUR_WINDOW:
        {
            wind = full_screen->get_active_window();     
            break;
        }
        case SCREEN_TYPE::DRAG:
        case SCREEN_TYPE::CUR_MONITOR:
        {
            auto active_window = full_screen->get_active_window();
            int mon = full_screen->get_monitor_at_window(active_window);

            full_screen->get_monitor_geometry(mon, crop_pos);

            crop = true;
        }
        case SCREEN_TYPE::FULL:
        {
            auto display = full_screen->get_display();
            wind         = full_screen->get_root_window();
            break;
        }

    }

    screen = Gdk::Pixbuf::create(
            wind, 0, 0, 
            wind->get_width(),
            wind->get_height()
    );

    if(crop) {
        auto temp = Rectangle{
            crop_pos.get_x(),
            crop_pos.get_y(),
            crop_pos.get_width(),
            crop_pos.get_height()
        };

        this->crop(temp);
    }

    auto std_timestamp = std::time(nullptr);
    auto localtime = *std::localtime(&std_timestamp);
    std::stringstream temp;
    temp << std::put_time(&localtime, "%c");

    time_stamp = temp.str();
}

void ScreenShot::crop(Rectangle cropRect)
{

    screen = screen->create_subpixbuf(
            screen,
            cropRect.x,
            cropRect.y,
            cropRect.w,
            cropRect.h
    );
}

void ScreenShot::write() const
{
    std::string save_path = ResourceManager().getSettingsValue("savepath");

    if(!std::filesystem::exists(save_path)) {
        logger->error("Unable to save to {}. Path does not exist.", save_path); 
        return;
    }

    std::string img_type  = ResourceManager().getSettingsValue(FILE_TYPE);

    std::filesystem::path filename(save_path+time_stamp+"."+img_type);

    try{
        screen->save(filename, img_type);
    }catch(Gdk::PixbufError &err) {
        logger->error("Unsupported filetype: {}", img_type);
        return;
    }

    logger->info("Image written: {}", filename.c_str());
}

void ScreenShot::upload()
{
    curlpp::Cleanup cleaner;

    logger->info("Uploading image...");
    curlpp::Easy request;

    std::string img_type   = ResourceManager().getSettingsValue(FILE_TYPE);
    std::string auth_token = ResourceManager().getSettingsValue(AUTH_TOKEN);
    std::string IMGUR_API  = "https://api.imgur.com/3/image.json";

    char *buff = nullptr;
    gsize size = 0;

    screen->save_to_buffer(buff, size, img_type);
    std::string img_b64 = b64::encode(buff, size);

    delete[] buff;

    using namespace curlpp::options;

    std::list<std::string> headers;
    headers.push_back("Authorization: " + auth_token);

    {
        curlpp::Forms form_data;
        form_data.push_back(new curlpp::FormParts::Content("image", img_b64));
        form_data.push_back(new curlpp::FormParts::Content("title", time_stamp));

        request.setOpt<HttpPost>(form_data);
    }

    request.setOpt<HttpHeader>(headers);
    request.setOpt<Url>(IMGUR_API);

    json data_ret;
    std::stringstream str;
    str << request;

    data_ret = data_ret.parse(str.str());

    if(data_ret["success"].get<bool>()) {
        url         = data_ret["data"]["link"];
        delete_hash = data_ret["data"]["deletehash"]; 

        logger->info("Success: {}", url);

        auto refClipboard = Gtk::Clipboard::get();
        refClipboard->set_text(url);
        refClipboard->store();

        ResourceManager().addScreen(*this);
    }else{
        std::string error_msg    = data_ret["data"]["error"];
        int         error_status = data_ret["status"]; 

        logger->error(
                "Image failed to upload:\n\tStatus: {}\n\tError: {}", 
                error_status,
                error_msg
        );

        auto app_sender = Gio::Application::create("io.bittwise.screens.notif", Gio::APPLICATION_FLAGS_NONE);
        app_sender->register_application();
        auto notif = Gio::Notification::create("Error Occurred");
        notif->set_body(error_msg);
        app_sender->send_notification(notif);
    }
}

Glib::RefPtr<Gdk::Pixbuf> ScreenShot::get_screen() {
    return screen;
}

ScreenShot::~ScreenShot()
{
}

void to_json(json &j, const ScreenShot &scr)
{
    j = json{{"date",   scr.time_stamp},
             {"delete", scr.delete_hash},
             {"url",    scr.url}};
}
