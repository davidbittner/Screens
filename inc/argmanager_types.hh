#pragma once

#include <exception>
#include <string>
#include <optional>

namespace argmanager {
    struct arg_info {
        bool required;
        bool has_arg;

        std::string s_name;
        std::string l_name;

        std::string desc;
    };

    struct arg {
        std::optional<std::string> param;
    };

    struct arg_exception : public std::exception {
        const char * what () const throw ();
        std::string print();

        arg_exception(std::string arg_name, std::string error);

        std::string arg_name;
        std::string error;
    };
};
