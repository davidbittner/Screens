#include "mainwindow.hh"
#include "settings.hh"

#include <fstream>
#include <iostream>

MainWindow::MainWindow() :
    imageTab(Gtk::ORIENTATION_HORIZONTAL),
    infoWindow(imageListStore, &imageModel)
{
    set_border_width(2);
    set_default_size(600, 400);

    add(notebook);

    notebook.set_border_width(2);

    notebook.append_page(imageTab, "Screenshots");
    notebook.append_page(settingsTab, "Settings");

    imageListStore = Gtk::ListStore::create(imageModel);
    imageList.set_model(imageListStore);
    imageList.append_column("Date", imageModel.date);

    //Load in past screenshots as items in the treeview
    for(json i : ResourceManager().imageDump())
    {
        auto row = *imageListStore->append();
        row[imageModel.date]       = i["date"].get<std::string>();
        row[imageModel.url]        = i["url"].get<std::string>();
        row[imageModel.deleteHash] = i["delete"].get<std::string>();
    }

    imageListHolder.set_size_request(200);

    imageInfoBox.pack_start(infoWindow, Gtk::PACK_EXPAND_WIDGET);

    imageListHolder.add(imageList);
    imageTab.add1(imageListHolder);
    imageTab.add2(imageInfoBox);

    imageList.signal_row_activated().connect(sigc::mem_fun(*this, &MainWindow::rowChanged));

    show_all_children();
}

void MainWindow::rowChanged(const Gtk::TreeModel::Path &path, Gtk::TreeViewColumn *column)
{
    infoWindow.setInfo(path);
}

MainWindow::~MainWindow()
{
}
