#pragma once

namespace b64 {
    std::string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    std::string encode(char *data, int len) {
        std::string encoded;
        char *digits;

        char characters[4];
        for(int i = 0; i < len/3; i++) {
            digits = (char *)(data + (i*3));
        
            characters[0] =  (digits[0] & 0xFC) >> 2;
            characters[1] = ((digits[0] & 0x03) << 4) | ((digits[1] & 0xF0) >> 4); 
            characters[2] = ((digits[1] & 0x0F) << 2) | ((digits[2] & 0xC0) >> 6);
            characters[3] =  (digits[2] & 0x3f);

            for(int j = 0; j < 4; j++) {
                encoded.push_back(_chars[characters[j]]);
            }
        }

        int remainder = len%3;
        char temp_chars[3];
        if(remainder) {
            digits = (char *)(data + ((len/3)*3));

            characters[0] =  (digits[0] & 0xFC) >> 2;
            characters[1] = ((digits[0] & 0x03) << 4) | ((digits[1] & 0xF0) >> 4); 
            characters[2] = ((digits[1] & 0x0F) << 2) | (0);

            for(int i = 0; i < remainder + 1; i++) {
                encoded.push_back(_chars[characters[i]]);
            }

            for(int i = 0; i < ((remainder == 1)?(2):(1)); i++) {
                encoded.push_back('=');
            }
        }

        return encoded;
    }
};
