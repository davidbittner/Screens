#include "settingstab.hh"

SettingsTab::SettingsTab() :
    saveButton("Save")
{
    settings_list.push_back(SettingsEntry("Save Path:", SAVE_PATH));
    settings_list.push_back(SettingsEntry("File Type:", FILE_TYPE));

    for(auto &item : settings_list) {
        item.add(*this);
    }

    pack_end(saveButton, Gtk::PACK_SHRINK, 5);
    saveButton.signal_pressed().connect(sigc::mem_fun(*this, &SettingsTab::savePressed));
}

void SettingsTab::savePressed()
{
    ResourceManager accessor;

    for(auto &item : settings_list) {
        item.save(accessor);
    }

    accessor.saveSettings();
}

SettingsTab::SettingsEntry::SettingsEntry(std::string name, std::string setting_name) :
    label(name)
{
    val.set_text(ResourceManager().getSettingsValue(setting_name));

    holder.pack_start(label, Gtk::PACK_SHRINK, 5);
    holder.pack_end(val, Gtk::PACK_EXPAND_WIDGET, 5);

    this->setting_name = setting_name;
}

void SettingsTab::SettingsEntry::add(Gtk::VBox &cont)
{
    cont.pack_start(holder, Gtk::PACK_SHRINK, 5);
}

void SettingsTab::SettingsEntry::save(ResourceManager &to)
{
     to.setSettingsValue(setting_name, val.get_text());
}
