#include "dragger.hh"

#include <iostream>
#include <gtkmm.h>
#include <future>
#include <gstreamermm.h>

#include "screenshot.hh"
#include "settings.hh"
#include "argmanager.hh"

Dragger::Dragger() :
    writeDone(false),
    uploadDone(false)
{
    cropShot = new ScreenShot(SCREEN_TYPE::DRAG);

    x1 = -1; y1 = -1;

    both = StaticManager().hadArg("--write") &&
           StaticManager().hadArg("--upload");

    fullscreen();
    set_app_paintable(true);

    signal_button_press_event().connect(sigc::mem_fun(this, &Dragger::mousePress));
    signal_button_release_event().connect(sigc::mem_fun(this, &Dragger::mousePress));

    set_accept_focus(false);

    set_size_request(16384, 16384);

    realize();
    show();
}

Rectangle upperLeft( int x1, int x2, int y1, int y2 )
{
    Rectangle ret;

    //This is purely for finding proper orientation for the rectangle
    //This matters which way they drag the rectangle, essentially.
    if(x1<x2)
    {
        ret.x = x1;
        ret.w = x2-x1;
    }else
    {
        ret.x = x2;
        ret.w = x1-x2;
    }

    if(y1<y2)
    {
        ret.y = y1;
        ret.h = y2-y1;
    }else
    {
        ret.y = y2;
        ret.h = y1-y2;
    }

    return ret;
}

bool Dragger::isDone()
{
    return (both)?(writeDone && uploadDone):(writeDone || uploadDone);
}

bool Dragger::mousePress(GdkEventButton *event)
{
    switch(event->type)
    {
        case GDK_BUTTON_PRESS:
        {
            if(event->button == 1 /*Left mouse button*/)
            {
                x1 = event->x;
                y1 = event->y;
            }
            break;
        }
        case GDK_BUTTON_RELEASE:
        {
            if( event->button == 1 )
            {
                x2 = event->x;
                y2 = event->y;

                process();
                close();
            }
            break;
        }
        default: break;
    }
    return true;
}

Dragger::~Dragger()
{
    //These if statements check the state of the finished threads
    if( StaticManager().hadArg("--write"))
    {
        if( !writeThread.get() )
        {
            std::cerr << "Write failed. Check permissions." << std::endl;
        }
    }
    if( StaticManager().hadArg("--upload"))
    {
        if( !uploadThread.get() )
        {
            std::cerr << "Upload failed, check connection." << std::endl;
        }
    }

    delete cropShot;
}

bool Dragger::on_draw(const Cairo::RefPtr<Cairo::Context> &cont)
{
    Gtk::Allocation allocation = get_allocation();  

    Gdk::Cairo::set_source_pixbuf(cont, cropShot->get_screen(), 0, 0);
    cont->rectangle(0, 0, allocation.get_width(), allocation.get_height());
    cont->fill();

    if(x1 != -1)
    {
        int x, y;
        get_pointer(x, y);

        cont->set_source_rgba(0.0, 0.0, 0.0, 0.5);
        cont->rectangle(x1, y1, (x-x1), (y-y1));
        cont->fill();
    }

    return true;
}

void Dragger::process()
{               

    //The position of the window is grabbed to properly orient cropping
    int wx, wy;
    get_position(wx, wy);

    selectArea = upperLeft( x1, x2, y1, y2 );

    if(selectArea.w <= 5 || selectArea.h <= 5)
    {
        uploadDone = true;
        writeDone  = true;
        std::cerr << "Image too small" << std::endl;
        return;
    }

    cropShot->crop(selectArea);

    if(StaticManager().hadArg("--write"))
    {
        writeThread = std::async( std::launch::async, [&]{
            cropShot->write();
            writeDone = true;

            return true;
        });
    }
    if(StaticManager().hadArg("--upload"))
    {
        uploadThread = std::async( std::launch::async, [&]{
            cropShot->upload();
            uploadDone = true;

            return true;
        });
    }
}
