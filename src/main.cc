#include <iostream>
#include <cstring>
#include <gstreamermm.h>
#include <thread>
#include <chrono>

#include "screenshot.hh"
#include "argmanager.hh"
#include "dragger.hh"
#include "settings.hh"
#include "mainwindow.hh"

#include "argmanager_types.hh"

#include <X11/Xlib.h>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

static auto logger = spdlog::stdout_color_mt("main");

int main(int argc, char **argv)
{
    XInitThreads();
    Gst::init();

    StaticManager().addArg("-s", "--screen", "take a screenshot", false, false);
    StaticManager().addArg("-u", "--upload", "upload the screenshot online (does nothing without --screen)", false, false);
    StaticManager().addArg("-w", "--write",  "write the screenshot to disk (does nothing without --screen)", false, false);
    StaticManager().addArg("-d", "--drag",   "opens a dialog to crop a screenshot (does nothing without --screen)", false, false);
    StaticManager().addArg("-h", "--help",   "prints this help dialog", false, false);

    try{
        StaticManager().passArgs(argc, argv);
    }catch(argmanager::arg_exception &ex) {
        logger->error(ex.print());

        StaticManager().printHelp(std::cout);

        return 1;
    }

    if(StaticManager().hadArg("--help")) {
        StaticManager().printHelp(std::cout);
        return 0;
    }
    
    argc = 0;
    auto app = Gtk::Application::create(argc, argv, "io.bittwise.screens");

    if( StaticManager().hadArg("--screen") )
    {
        if(StaticManager().hadArg("--drag"))
        {
            int x1 = 0, x2 = 0;

            Dragger draggerWindow;
            auto mainInstance = Gtk::Main::instance();
            while( !draggerWindow.isDone() )
            {
                //Only draw the window if the cursor has changed position
                int tx1, tx2;
                draggerWindow.get_pointer(tx1, tx2);

                //If the cursor position changes, redraw rectangle.
                if( tx1 != x1 || tx2 != x2 )
                {
                    draggerWindow.queue_draw();
                    x1 = tx1;
                    x2 = tx2;
                }

                mainInstance->iteration(false);

                //Limit so it doesn't run faster than need be
                std::this_thread::sleep_for(std::chrono::milliseconds(2));
            }
        }else{
            ScreenShot fullScreenShot(SCREEN_TYPE::CUR_MONITOR);
            
            std::future<bool> writeThread;
            std::future<bool> uploadThread;
            if( StaticManager().hadArg("--write"))
            {
                writeThread = std::async(std::launch::async, [&]()
                {
                    fullScreenShot.write();
                    return true;
                });
            }

            if( StaticManager().hadArg("--upload"))
            {
                uploadThread = std::async(std::launch::async, [&]()
                {
                    fullScreenShot.upload();
                    return true;
                });
            }

            if( StaticManager().hadArg("--write"))
            {
                writeThread.get();
            }
            if( StaticManager().hadArg("--upload"))
            {
                uploadThread.get();
            }
        }
    }else {
        MainWindow mainWindow;

        return app->run(mainWindow);
    }

    return 0;
}
