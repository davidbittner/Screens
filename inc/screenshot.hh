#ifndef SCREENSHOT_HEADER
#define SCREENSHOT_HEADER

#include <gtkmm.h>
#include <gdkmm/pixbuf.h>
#include <string>

#include <cstdint>
#include <vector>
#include <future>

#include "rect.hh"
#include "json.hpp"

enum class SCREEN_TYPE {
    DRAG,
    FULL,
    CUR_WINDOW,
    CUR_MONITOR
};

using json = nlohmann::json;

class ScreenShot
{
public:
     ScreenShot(SCREEN_TYPE type);
    ~ScreenShot();

    void  write() const;
    void upload();

    void crop(Rectangle cropRect);
    Glib::RefPtr<Gdk::Pixbuf> get_screen();
    
    friend void to_json(json &j, const ScreenShot &scr);
private:
    std::string time_stamp;

    //The upload url
    std::string url;
    std::string delete_hash;

    Glib::RefPtr<Gdk::Pixbuf> screen;

};

#endif
